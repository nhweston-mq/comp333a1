package org.bitbucket.nhweston.comp333a1a.test;

import org.bitbucket.nhweston.comp333a1a.AdjacencyList;
import org.bitbucket.nhweston.comp333a1a.Graph;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class AdjacencyListTests {

    @Test
    public void constructor() {
        Graph<String> g = new AdjacencyList<>();
    }

    @Test
    public void single_vertex() {
        Graph<String> g = new AdjacencyList<>();
        g.addVertex("v");
        Collection<String> actual = new HashSet<>(g.vertices());
        Collection<String> expected = new HashSet<>(Collections.singletonList("v"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void many_vertices() {
        Graph<String> g = new AdjacencyList<>();
        g.addVertex("v1");
        g.addVertex("v2");
        g.addVertex("v3");
        Collection<String> actual = new HashSet<>(g.vertices());
        Collection<String> expected = new HashSet<>(Arrays.asList("v1", "v2", "v3"));
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void single_edge() {
        Graph<String> g = new AdjacencyList<>();
        g.addVertex("v1");
        g.addVertex("v2");
        g.addEdge("v1", "v2");
        Collection<String> actualV1 = new HashSet<>(g.adjacencies("v1"));
        Collection<String> expectedV1 = new HashSet<>(Collections.singletonList("v2"));
        Collection<String> actualV2 = new HashSet<>(g.adjacencies("v2"));
        Collection<String> expectedV2 = new HashSet<>(Collections.singletonList("v1"));
        Assert.assertEquals(expectedV1, actualV1);
        Assert.assertEquals(expectedV2, actualV2);
    }

}
