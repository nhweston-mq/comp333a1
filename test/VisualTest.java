package org.bitbucket.nhweston.comp333a1a.test;

import org.bitbucket.nhweston.comp333a1a.AdjacencyList;
import org.bitbucket.nhweston.comp333a1a.Graph;
import org.bitbucket.nhweston.comp333a1a.test.visual.DisplayTest;
import org.bitbucket.nhweston.comp333a1a.test.visual.Vertex;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;

public class VisualTest {

    static Random rng = new Random();

    static int centreX = 320;
    static int centreY = 400;
    static int size = 24;
    static int radius = 240;

    static int millisPerFrame = 100;

    static int partite = 7;
    static int numVertices = 40;
    static BiFunction<Double, Double, Double> chanceDist = (a, b) -> 0.5;

    static List<Vertex> vertices;

    public static void main(String[] args) {
        Graph<Vertex> graph = new AdjacencyList<>();
        vertices = new ArrayList<>();
        for (int i=0; i<numVertices; i++) {
            Vertex v = new Vertex(0, 0, size);
            graph.addVertex(v);
            vertices.add(v);
            for (int j=0; j<i; j++) {
                if (rngCheck(i, j) && partiteCheck(i, j)) {
                    graph.addEdge(v, vertices.get(j));
                }
            }
        }
        vertices.sort(Comparator.comparingInt(graph::degree).reversed().thenComparing(Comparator.comparingInt(Object::hashCode)));
        for (int i=0; i<numVertices; i++) {
            int posX = (int) (centreX + radius*Math.cos(2*Math.PI*i/numVertices));
            int posY = (int) (centreY + radius*Math.sin(2*Math.PI*i/numVertices));
            vertices.get(i).setPosX(posX);
            vertices.get(i).setPosY(posY);
        }
        DisplayTest test = new DisplayTest(graph, millisPerFrame);
        test.run();
    }

    static boolean rngCheck(int i, int j) {
        double chance = chanceDist.apply(i*1.00/(numVertices-1), j*1.00/(numVertices-1));
        return rng.nextDouble() < chance;
    }

    static boolean partiteCheck(int i, int j) {
        return i%partite != j%partite;
    }

}
