package org.bitbucket.nhweston.comp333a1a.test;

import org.bitbucket.nhweston.comp333a1a.AdjacencyList;
import org.bitbucket.nhweston.comp333a1a.Graph;
import org.bitbucket.nhweston.comp333a1a.GraphColoring;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class TimedTests {

    static final int MAX_VERTICES = 50;

    static Random rng = new Random();

    static List<String> lines = new ArrayList<>();

    static Graph<Integer> g;
    static GraphColoring<Integer> gc;

    static long test(int numVertices, double completeness) {
        g = new AdjacencyList<>();
        for (int i=0; i<numVertices; i++) {
            g.addVertex(i);
            for (int j=0; j<i; j++) if (rng.nextDouble() < completeness) {
                g.addEdge(i, j);
            }
        }
        gc = new GraphColoring<>(g);
        Instant start = Instant.now();
        gc.run();
        Instant end = Instant.now();
        return Duration.between(start, end).toMillis();
    }

    public static void main(String[] args) {
        Instant end = Instant.now().plusSeconds(300);
        for (int numVertices=0; numVertices<=MAX_VERTICES; numVertices++) {
            lines.add("");
            lines.add("* * *");
            lines.add("VERTICES: " + numVertices);
            lines.add("");
            for (double completeness=0; completeness<1.01; completeness=completeness+0.05) {
                String strNumVertices = String.format("%6d", numVertices);
                String strCompleteness = String.format("%6.2f", completeness);
                long timeMin = Long.MAX_VALUE;
                long timeMean = 0;
                long timeMax = Long.MIN_VALUE;
                long sSMin = Long.MAX_VALUE;
                long sSMean = 0;
                long sSMax = Long.MIN_VALUE;
                long sIMin = Long.MAX_VALUE;
                long sIMean = 0;
                long sIMax = Long.MIN_VALUE;
                for (int trial=0; trial<16; trial++) {
                    long time = test(numVertices, completeness);
                    long sS = gc.stepsSearchLoop();
                    long sI = gc.stepsInterferenceLoop();
                    if (time < timeMin) timeMin = time;
                    if (time > timeMax) timeMax = time;
                    if (sS < sSMin) sSMin = sS;
                    if (sS > sSMax) sSMax = sS;
                    if (sI < sIMin) sIMin = sI;
                    if (sI > sIMax) sIMax = sI;
                    timeMean = timeMean + time;
                    sSMean = sSMean + sS;
                    sIMean = sIMean + sI;
                    String strTrial = String.format("%6d", trial);
                    String strTime = String.format("%8d ms", time);
                    System.out.println(strNumVertices + strCompleteness + strTrial + strTime);
                }
                timeMean = timeMean / 16;
                sSMean = sSMean / 16;
                sIMean = sIMean / 16;
                String strTimeMin = String.format("%8d ms", timeMin);
                String strTimeMean = String.format("%8d ms", timeMean);
                String strTimeMax = String.format("%8d ms", timeMax);
                String strSSMin = String.format("%,12d", sSMin);
                String strSSMean = String.format("%,12d", sSMean);
                String strSSMax = String.format("%,12d", sSMax);
                String strSIMin = String.format("%,12d", sIMin);
                String strSIMean = String.format("%,12d", sIMean);
                String strSIMax = String.format("%,12d", sIMax);
                String str = " |" + strNumVertices + strCompleteness + " |" + strTimeMean + strTimeMin + strTimeMax +
                        " |" + strSSMean + strSSMin + strSSMax + " |" + strSIMean + strSIMin + strSIMax + " |";
                lines.add(str);
            }
        }
        write(lines);
    }

    public static void write(List<String> lines) {
        try {
            PrintWriter writer = new PrintWriter(new FileWriter("data/timed_tests.txt", false));
            for (String line : lines) {
                writer.printf("%s" + "%n", line);
            }
            writer.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

}
