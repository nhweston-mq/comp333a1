package org.bitbucket.nhweston.comp333a1a.test;

import org.bitbucket.nhweston.comp333a1a.AdjacencyList;
import org.bitbucket.nhweston.comp333a1a.Graph;
import org.bitbucket.nhweston.comp333a1a.GraphColoring;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AverageTests {

    static long test(int numVertices, double completeness) {
        g = new AdjacencyList<>();
        for (int i=0; i<numVertices; i++) {
            g.addVertex(i);
            for (int j=0; j<i; j++) if (rng.nextDouble() < completeness) {
                g.addEdge(i, j);
            }
        }
        gc = new GraphColoring<>(g);
        gc.run();
        return gc.stepsInterferenceLoop();
    }

    static Random rng = new Random();

    static Graph<Integer> g;
    static GraphColoring<Integer> gc;
    static List<Long> results;

    public static void main(String[] args) {
        results = new ArrayList<>();
        for (int i=0; i<1000; i++) {
            results.add(test(20, 0.9));
        }
        results.sort(Long::compare);
        for (Long i : results) {
            System.out.println(i);
        }
    }

}
