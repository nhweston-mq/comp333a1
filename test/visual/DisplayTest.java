package org.bitbucket.nhweston.comp333a1a.test.visual;

import org.bitbucket.nhweston.comp333a1a.Graph;

import javax.swing.*;

import java.awt.*;
import java.time.Duration;
import java.time.Instant;

public class DisplayTest extends JFrame implements Runnable {

    private static final Font FONT = new Font("Arial", Font.PLAIN, 24);

    private class Canvas extends JPanel {

        public Canvas() {
            setPreferredSize(new Dimension(1280, 720));
        }

        @Override
        public void paint(Graphics g) {
            g.setFont(FONT);
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, 1280, 720);
            active = gcd.paint(g);
        }

    }

    private GraphColoringDisplay gcd;
    private boolean active;
    private int timePerFrame;

    public DisplayTest(Graph<Vertex> graph, int timePerFrame) {
        gcd = new GraphColoringDisplay(graph);
        this.timePerFrame = timePerFrame;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        Canvas canvas = new Canvas();
        setContentPane(canvas);
        pack();
        setVisible(true);
    }

    @Override
    public void run() {
        gcd.run();
        System.out.println("Frames: " + gcd.getNumFrames());
        System.out.println("Solution: " + gcd.numColorsOptimal());
        active = true;
        while (active) {
            Instant startTime = Instant.now();
            this.repaint();
            Instant endTime = Instant.now();
            long time = timePerFrame - Duration.between(startTime, endTime).toMillis();
            if (time > 0) try {
                Thread.sleep(time);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
