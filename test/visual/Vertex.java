package org.bitbucket.nhweston.comp333a1a.test.visual;

import java.awt.*;

public class Vertex implements Comparable<Vertex> {

    private static final Color COLOR_FILL_DEFAULT = new Color(128, 128, 128);
    private static final Color COLOR_STROKE_DEFAULT = new Color(255, 255, 255);
    private static final int SIZE_STROKE_DEFAULT = 4;

    private static int serial = 0;

    private int id;
    private int posX, posY;
    private int size, sizeStroke;
    private Color colorFill, colorOptimal, colorStroke;

    public Vertex(int posX, int posY, int size) {
        this(serial, posX, posY, size);
        serial++;
    }

    public Vertex(int id, int posX, int posY, int size) {
        this.id = id;
        this.posX = posX;
        this.posY = posY;
        this.size = size;
        sizeStroke = SIZE_STROKE_DEFAULT;
        colorFill = COLOR_FILL_DEFAULT;
        colorOptimal = COLOR_FILL_DEFAULT;
        colorStroke = COLOR_STROKE_DEFAULT;
    }

    public void paint(Graphics g, int xOffset) {
        g.setColor(colorStroke);
        g.fillOval(posX+xOffset-size-sizeStroke/2, posY-size-sizeStroke/2, 2*size+sizeStroke, 2*size+sizeStroke);
        g.setColor(colorOptimal);
        g.fillOval(posX+xOffset-size, posY-size, 2*size, 2*size);
    }

    public void paint(Graphics g) {
        g.setColor(colorStroke);
        g.fillOval(posX-size-sizeStroke/2, posY-size-sizeStroke/2, 2*size+sizeStroke, 2*size+sizeStroke);
        g.setColor(colorFill);
        g.fillOval(posX-size, posY-size, 2*size, 2*size);
    }

    public void setColorFill(Color colorFill) {
        this.colorFill = colorFill;
    }

    public void setColorOptimal(Color colorOptimal) {
        this.colorOptimal = colorOptimal;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    @Override
    public int compareTo(Vertex other) {
        return Integer.compare(this.id, other.id);
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "" + id;
    }

}
