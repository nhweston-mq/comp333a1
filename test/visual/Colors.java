package org.bitbucket.nhweston.comp333a1a.test.visual;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Colors {

    static Random rng = new Random();

    static List<Color> colors;

    static Color[] colorsAsArray = {
            new Color(128, 128, 128),
            new Color(192,   0,   0),
            new Color(  0, 192,   0),
            new Color(  0,   0, 192),
            new Color(224, 224,   0),
            new Color(224,   0, 224),
            new Color(  0, 224, 224),
            new Color(255, 128,   0),
            new Color(255,   0, 128),
            new Color(128, 255,   0),
            new Color(  0, 255, 128),
            new Color(128,   0, 255),
            new Color(  0, 128, 255),
            new Color(128,  64,   0),
            new Color(128,   0,  64),
            new Color( 64, 128,   0),
            new Color(  0, 128,  64),
            new Color( 64,   0, 128),
            new Color(  0,  64, 128),
            new Color(192, 128,  64),
            new Color(192,  64, 128),
            new Color(128, 192,  64),
            new Color( 64, 192, 128),
            new Color(128,  64, 192),
            new Color( 64, 128, 192)
    };

    static {
        colors = new ArrayList<>(Arrays.asList(colorsAsArray));
    }

    static Color get(int index) {
        while (index >= colors.size()) {
            colors.add(new Color(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256)));
        }
        return colors.get(index);
    }

}
