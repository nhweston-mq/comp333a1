package org.bitbucket.nhweston.comp333a1a.test.visual;

import org.bitbucket.nhweston.comp333a1a.Graph;
import org.bitbucket.nhweston.comp333a1a.GraphColoring;

import java.awt.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class GraphColoringDisplay extends GraphColoring<Vertex> {

    private static final int OPTIMAL_OFFSET_DEFAULT = 640;
    private static final Color COLOR_EDGE_DEFAULT = Color.WHITE;

    class Frame {

        Vertex vertex;
        Color color;
        Map<Vertex, Color> optimal;
        int numColsOpt;

        Frame(Vertex vertex, Color color) {
            this.vertex = vertex;
            this.color = color;
            numColsOpt = -1;
        }

        void setOptimal(int[] optimal) {
            this.optimal = new Hashtable<>();
            int max = -1;
            for (int i=0; i<optimal.length; i++) {
                this.optimal.put(verticesByIndex.get(i), Colors.get(optimal[i]));
                if (optimal[i] > max) max = optimal[i];
            }
            numColsOpt = max;
        }

    }

    private List<Frame> frames;
    private int frame;
    private int optimalOffset;
    private int numColsOpt;

    public GraphColoringDisplay(Graph<Vertex> graph) {
        super(graph);
        frames = new ArrayList<>();
        frame = 0;
        optimalOffset = OPTIMAL_OFFSET_DEFAULT;
        numColsOpt = -1;
    }

    public boolean paint(Graphics g) {
        // Paint edges.
        g.setColor(COLOR_EDGE_DEFAULT);
        for (Vertex v1 : graph.vertices()) {
            for (Vertex v2 : graph.adjacencies(v1)) {
                g.drawLine(v1.getPosX(), v1.getPosY(), v2.getPosX(), v2.getPosY());
                g.drawLine(optimalOffset + v1.getPosX(), v1.getPosY(), optimalOffset + v2.getPosX(), v2.getPosY());
            }
        }
        // Update main graph.
        Frame current = frames.get(frame);
        current.vertex.setColorFill(current.color);
        // Update optimal graph.
        if (current.optimal != null) {
            for (Vertex v : graph.vertices()) {
                v.setColorOptimal(current.optimal.get(v));
            }
            numColsOpt = current.numColsOpt;
        }
        // Draw text for optimal graph.
        if (numColsOpt >= 0) {
            g.drawString("Minimum colors: " + numColsOpt, OPTIMAL_OFFSET_DEFAULT, 64);
        }
        g.drawString("Step: " + frame, 64, 64);
        // Draw vertices.
        for (Vertex v : graph.vertices()) {
            v.paint(g);
            v.paint(g, optimalOffset);
        }
        frame++;
        return frame < frames.size();
    }

    @Override
    protected void setCols(int vertexId, int col) {
        super.setCols(vertexId, col);
        frames.add(new Frame(verticesByIndex.get(vertexId), Colors.get(col)));
    }

    @Override
    protected void setColorsOptimal(int[] colorsOptimal) {
        super.setColorsOptimal(colorsOptimal);
        frames.get(frames.size()-1).setOptimal(colorsOptimal);
    }

    public int getNumFrames() {
        return frames.size()-1;
    }

}
