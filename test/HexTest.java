package org.bitbucket.nhweston.comp333a1a.test;

import org.bitbucket.nhweston.comp333a1a.AdjacencyList;
import org.bitbucket.nhweston.comp333a1a.Graph;
import org.bitbucket.nhweston.comp333a1a.GraphColoring;
import org.bitbucket.nhweston.comp333a1a.test.visual.GraphColoringDisplay;
import org.bitbucket.nhweston.comp333a1a.test.visual.Vertex;

import java.util.*;

public class HexTest {

    static Random rng = new Random();

    static int centreX = 320;
    static int centreY = 400;
    static int size = 64;
    static int radius = 200;
    static int numVertices = 10;
    static List<Vertex> vertices = new ArrayList<>();

    static int maxDegree;
    static int numGraphs;

    static {
        for (int i=0; i<numVertices; i++) {
            vertices.add(new Vertex(i, getPosX(i), getPosY(i), size));
        }
        maxDegree = (numVertices * (numVertices - 1)) / 2;
        numGraphs = (int) Math.pow(2, maxDegree);
    }

    static int getPosX(int id) {
        return (int) (centreX + radius*Math.cos(2*Math.PI*id/numVertices));
    }

    static int getPosY(int id) {
        return (int) (centreY + radius*Math.sin(2*Math.PI*id/numVertices));
    }

    static Graph<Vertex> gen(int id) {
        boolean[] edgeExists = new boolean[maxDegree];
        for (int i=0; i<maxDegree; i++) {
            edgeExists[i] = (id%2) == 1;
            id = id / 2;
        }
        return gen(edgeExists);
    }

    static Graph<Integer> genInt(int id) {
        boolean[] edgeExists = new boolean[maxDegree];
        for (int i=0; i<maxDegree; i++) {
            edgeExists[i] = (id%2) == 1;
            id = id / 2;
        }
        return genInt(edgeExists);
    }

    static Graph<Vertex> gen(boolean[] edgeExists) {
        vertices = new ArrayList<>();
        Graph<Vertex> result = new AdjacencyList<>();
        for (int i=0; i<numVertices; i++) {
            Vertex v = new Vertex(i, getPosX(i), getPosY(i), size);
            vertices.add(v);
            result.addVertex(v);
        }
        int edgeId = 0;
        for (int i=0; i<numVertices; i++) {
            Vertex iV = vertices.get(i);
            for (int j=0; j<i; j++) {
                Vertex jV = vertices.get(j);
                if (edgeExists[edgeId]) {
                    result.addEdge(iV, jV);
                }
                else {
                    result.removeEdge(iV, jV);
                }
                edgeId++;
            }
        }
        System.out.println(result.toString());
        vertices.sort(Comparator.comparingInt(result::degree).reversed().thenComparing(Comparator.comparingInt(Object::hashCode)));
        int i = 0;
        for (Vertex v : vertices) {
            v.setPosX(getPosX(i));
            v.setPosY(getPosY(i));
            i++;
        }
        return result;
    }

    static Graph<Integer> genInt(boolean[] edgeExists) {
        Graph<Integer> result = new AdjacencyList<>();
        for (int i=0; i<numVertices; i++) {
            result.addVertex(i);
        }
        int edgeId = 0;
        for (int i=0; i<numVertices; i++) {
            Vertex iV = vertices.get(i);
            for (int j=0; j<i; j++) {
                if (edgeExists[edgeId]) {
                    result.addEdge(i, j);
                }
                edgeId++;
            }
        }
        return result;
    }

    static String boolArrToString(boolean[] arr) {
        StringBuilder result = new StringBuilder();
        for (boolean bool : arr) {
            if (bool) {
                result.append("1");
            } else {
                result.append("0");
            }
        }
        return result.toString();
    }

    /*
     * 6:   Graph #1196
     */

    public static void main(String[] args) {
//        DisplayTest displayTest = new DisplayTest(gen(262659806), 500);
//        displayTest.run();
//        testWithId();
        Graph<Integer> g = new AdjacencyList<>();
        for (int i=0; i<10; i++) {
            g.addVertex(i);
        }
        g.addEdges(0, Arrays.asList(5, 6, 7, 8, 9));
        g.addEdges(1, Arrays.asList(4, 5, 6, 7, 8));
        g.addEdges(2, Arrays.asList(3, 4, 5, 6, 7));
        g.addEdges(3, Arrays.asList(2, 4, 5, 6, 7));
    }

    static void testWithRng() {
        int max = -1;
        for (int i=0; i<Integer.MAX_VALUE; i++) {
            boolean[] edgeExists = new boolean[maxDegree];
            for (int j=0; j<maxDegree; j++) {
                edgeExists[j] = rng.nextBoolean();
            }
            Graph<Vertex> graph = gen(edgeExists);
            GraphColoringDisplay gcd = new GraphColoringDisplay(graph);
            gcd.run();
            int numFrames = gcd.getNumFrames();
            if (numFrames > max) {
                max = numFrames;
                System.out.println("Graph " + boolArrToString(edgeExists) + " solved in " + numFrames + " steps.");
                for (Vertex v : vertices) {
                    StringBuilder line = new StringBuilder();
                    line.append(v.hashCode());
                    line.append(": ");
                    for (Vertex other : graph.adjacencies(v)) {
                        line.append(other.hashCode());
                        line.append(" ");
                    }
                    System.out.println(line.toString());
                }
            }
        }
    }

    static void testWithId() {
        long max = -1;
        for (int i=numGraphs-1; i>=0; i--) {
//            boolean[] edgeExists = new boolean[maxDegree];
//            for (int j=0; j<maxDegree; j++) {
//                edgeExists[j] = rng.nextDouble() < i*1.0/100000;
//            }
            Graph<Integer> graph = genInt(i);
            GraphColoring<Integer> gcd = new GraphColoring<>(graph);
            gcd.run();
            long stepsSearch = gcd.stepsSearchLoop();
            long stepsInterference = gcd.stepsInterferenceLoop();
            if (stepsInterference >= max) {
                max = stepsInterference;
                System.out.println(
                        "Graph " + i + " solved in " + stepsInterference + " interference steps, and " +
                        stepsSearch + " search steps. Solution has " + gcd.numColorsOptimal() + " colours."
                );
                System.out.println(graph.toString());
            }
        }
    }

}
