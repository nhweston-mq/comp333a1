package org.bitbucket.nhweston.comp333a1a.test;

import org.bitbucket.nhweston.comp333a1a.AdjacencyList;
import org.bitbucket.nhweston.comp333a1a.Graph;
import org.bitbucket.nhweston.comp333a1a.GraphColoring;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class GraphColoringTests {

    public void addVertices(Graph<String> g, Collection<String> vertices) {
        for (String v : vertices) {
            g.addVertex(v);
        }
    }

    public void addEdges(Graph<String> g, String vertex, Collection<String> adjacencies) {
        for (String v : adjacencies) {
            g.addEdge(vertex, v);
        }
    }

    public void test(Graph<String> g, int minCols) {
        GraphColoring<String> gc = new GraphColoring<>(g);
        gc.run();
        gc.get().forEach((v, c) -> System.out.println(v + ": " + c));
        Assert.assertEquals(minCols, gc.numColorsOptimal());
    }

    @Test
    public void v1() {
        Graph<String> g = new AdjacencyList<>();
        g.addVertex("v1");
        test(g, 1);
    }

    @Test
    public void v2_isolated() {
        Graph<String> g = new AdjacencyList<>();
        addVertices(g, Arrays.asList("v1", "v2"));
        test(g, 1);
    }

    @Test
    public void v2_complete() {
        Graph<String> g = new AdjacencyList<>();
        addVertices(g, Arrays.asList("v1", "v2"));
        g.addEdge("v1", "v2");
        test(g, 2);
    }

    @Test
    public void v3_isolated() {
        Graph<String> g = new AdjacencyList<>();
        addVertices(g, Arrays.asList("v1", "v2", "v3"));
        test(g, 1);
    }

    @Test
    public void v3_complete() {
        Graph<String> g = new AdjacencyList<>();
        addVertices(g, Arrays.asList("v1", "v2", "v3"));
        addEdges(g, "v1", Arrays.asList("v2", "v3"));
        addEdges(g, "v2", Collections.singletonList("v3"));
        test(g, 3);
    }

    @Test
    public void v3_split() {
        Graph<String> g = new AdjacencyList<>();
        addVertices(g, Arrays.asList("v1", "v2", "v3"));
        g.addEdge("v1", "v2");
        test(g, 2);
    }

    @Test
    public void v4_polygon() {
        Graph<String> g = new AdjacencyList<>();
        addVertices(g, Arrays.asList("v1", "v2", "v3", "v4"));
        g.addEdge("v1", "v2");
        g.addEdge("v2", "v3");
        g.addEdge("v3", "v4");
        g.addEdge("v4", "v1");
        test(g, 2);
    }

}
