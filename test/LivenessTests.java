package org.bitbucket.nhweston.comp333a1a.test;

import org.junit.Test;
import student.Liveness;

import java.util.TreeMap;

public class LivenessTests {

    String dir = "data/sample/";
    String extInput = ".dat";
    String extOutput = ".out.TEST";

    void test(String fileName) {
        TreeMap<String, Integer> solution;
        Liveness l = new Liveness();
        solution = l.generateSolution(dir + fileName + extInput);
        l.writeSolutionToFile(solution, dir + fileName + extOutput);
    }

    @Test
    public void test_ex1() {
        test("ex1");
    }

    @Test
    public void test_ex2() {
        test("ex2mod");
    }

    @Test
    public void test_ex3() {
        test("ex3mod");
    }

    @Test
    public void test_ex4() {
        test("ex4mod");
    }

}
