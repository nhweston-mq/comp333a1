package org.bitbucket.nhweston.comp333a1b;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser implements Runnable {

    private static final Pattern TOKEN  =   Pattern.compile("[a-zA-Z0-9\\-]");
    private static final Pattern VAR    =   Pattern.compile("[a-zA-Z][a-zA-Z0-9]");
    private static final Pattern EXPR   =   Pattern.compile("([a-zA-Z][a-zA-Z0-9])\\s*:=\\s*(.*)");
    private static final Pattern LIVE   =   Pattern.compile("live-((?:in)|(?:out))\\s+(.*)");

    private String filePath;
    private Graph<String> graph;
    private List<Collection<String>> subgraphsComplete;
    private Collection<String> subgraphMaximal;

    private Collection<String> live;

    public Parser(String filePath) {
        this.filePath = filePath;
        graph = new AdjacencyList<>();
        subgraphsComplete = new ArrayList<>();
        live = new HashSet<>();
    }

    @Override
    public void run() {
        Deque<String> lines = readFile();
        while (!lines.isEmpty()) {
            String line = lines.pollLast().trim();
            processLine(line);
            nextLine();
        }
    }

    public Graph<String> getGraph() {
        return graph;
    }

    public Collection<Collection<String>> getSubgraphsComplete() {
        return subgraphsComplete;
    }

    public Collection<String> getSubgraphMaximal() {
        return subgraphMaximal;
    }

    private Deque<String> readFile() {
        Deque<String> result = new LinkedList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
            reader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            return result;
        }
        return result;
    }

    private void processLine(String line) {
        Matcher matchExpr;      // Checks whether a line is an assignment expression.
        Matcher matchLive;      // Checks whether a line is a live-in or live-out declaration.
        Matcher matchTokens;    // Extracts tokens in an expression.
        Matcher matchVar;       // Checks whether a token is a variable.
        if ((matchExpr = EXPR.matcher(line)).matches()) {
            // Assignment expression.
            String varAssigned = matchExpr.group(1);
            String varsUsed = matchExpr.group(2);
            matchTokens = TOKEN.matcher(varsUsed);
            // Mark the variables on the RHS as used.
            while (matchTokens.find()) {
                String token = matchTokens.group();
                matchVar = VAR.matcher(token);
                if (matchVar.matches()) {
                    markUsed(token);
                }
            }
            // Mark the variable on the LHS as assigned.
            markAssigned(varAssigned);
        }
        else if ((matchLive = LIVE.matcher(line)).matches()) {
            // Live-in or live-out expression.
            String inOrOut = matchLive.group(1);
            String vars = matchLive.group(2);
            matchTokens = TOKEN.matcher(vars);
            if (inOrOut.equals("in")) while (matchTokens.find()) {
                String token = matchTokens.group();
                if (VAR.matcher(token).matches()) {
                    markAssigned(token);
                }
            }
            else if (inOrOut.equals("out")) while (matchTokens.find()) {
                String token = matchTokens.group();
                if (VAR.matcher(token).matches()) {
                    markUsed(token);
                }
            }
        }
    }

    private void markAssigned(String token) {
        live.remove(token);
    }

    private void markUsed(String token) {
        graph.addVertex(token);
        for (String other : live) {
            graph.addEdge(token, other);
        }
        live.add(token);
    }

    private void nextLine() {
        Collection<String> subgraph = new HashSet<>(live);
        subgraphsComplete.add(subgraph);
        if (subgraphMaximal == null || live.size() > subgraphMaximal.size()) {
            subgraphMaximal = subgraph;
        }
    }

}
