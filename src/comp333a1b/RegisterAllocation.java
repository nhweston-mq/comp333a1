package org.bitbucket.nhweston.comp333a1b;

import java.util.*;

public class RegisterAllocation<T> implements Runnable {

    private Graph<T> graph;
    private Collection<Collection<T>> subgraphsComplete;

    private Deque<Collection<T>> subgraphsIn;
    private Deque<Collection<T>> subgraphsOut;

    private int numVertices;
    private Map<T, Integer> indicesByVertex;
    private List<T> verticesByIndex;

    public RegisterAllocation(Graph<T> graph, Collection<Collection<T>> subgraphsComplete) {
        this.graph = graph;
        this.subgraphsComplete = subgraphsComplete;
    }

    @Override
    public void run() {
        sortSubgraphs();
    }

    private void search() {

    }

    private void sortSubgraphs() {
        Deque<Collection<T>> fromList = new LinkedList<>();
        Deque<Collection<T>> toList = new LinkedList<>();
        Collection<T> subgraphMaximal = new HashSet<>();
        for (Collection<T> subgraph : subgraphsComplete) {
            fromList.add(subgraph);
            if (subgraph.size() > subgraphMaximal.size()) {
                subgraphMaximal = subgraph;
            }
        }
        while (!fromList.isEmpty()) {
            Collection<T> redundancies = subgraphMaximal;
            subgraphMaximal = new HashSet<>();
            while (!fromList.isEmpty()) {
                Collection<T> subgraph = fromList.poll();
                subgraph.retainAll(redundancies);
                if (!subgraph.isEmpty()) {
                    toList.add(subgraph);
                    if (subgraph.size() > subgraphMaximal.size()) {
                        subgraphMaximal = subgraph;
                    }
                }
            }
            fromList = toList;
            toList = new LinkedList<>();
        }
        subgraphsComplete = toList;
    }

}
