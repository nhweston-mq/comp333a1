package org.bitbucket.nhweston.comp333a1b;

import java.util.*;

public class AdjacencyList<T> implements Graph<T> {

    private Map<T, Collection<T>> adjacencies;

    public AdjacencyList() {
        adjacencies = new LinkedHashMap<>();
    }

    @Override
    public boolean addVertex(T v) {
        if (adjacencies.containsKey(v)) return false;
        adjacencies.put(v, new LinkedHashSet<>());
        return true;
    }

    @Override
    public boolean removeVertex(T v) {
        if (!adjacencies.containsKey(v)) return false;
        adjacencies.remove(v);
        return true;
    }

    @Override
    public boolean addEdge(T v1, T v2) {
        if (!containsVertex(v1) || !containsVertex(v2) || containsEdge(v1, v2)) return false;
        adjacencies.get(v1).add(v2);
        adjacencies.get(v2).add(v1);
        return true;
    }

    @Override
    public boolean removeEdge(T v1, T v2) {
        if (!containsEdge(v1, v2)) return false;
        adjacencies.get(v1).remove(v2);
        adjacencies.get(v2).remove(v1);
        return true;
    }

    @Override
    public boolean containsVertex(T v) {
        return adjacencies.containsKey(v);
    }

    @Override
    public boolean containsEdge(T v1, T v2) {
        Collection<T> temp = adjacencies.get(v1);
        return (temp != null) && temp.contains(v2);
    }

    @Override
    public int degree(T v) {
        Collection<T> temp = adjacencies.get(v);
        return (temp == null) ? -1 : temp.size();
    }

    @Override
    public Collection<T> vertices() {
        return adjacencies.keySet();
    }

    @Override
    public Collection<T> adjacencies(T v) {
        return adjacencies.get(v);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<T, Collection<T>> entry : adjacencies.entrySet()) {
            result.append(entry.getKey().toString());
            result.append(": {");
            boolean first = true;
            for (T adj : entry.getValue()) {
                if (first) first = false;
                else result.append(", ");
                result.append(adj.toString());
            }
            result.append("}\n");
        }
        return result.toString();
    }

}
