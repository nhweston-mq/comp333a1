package org.bitbucket.nhweston.comp333a1b;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Liveness {

    public static void main(String[] args) {}

    public Liveness() {}

    public TreeMap<String, Integer> generateSolution(String fInName) {
//        Parser parser = new Parser(fInName);
//        parser.run();
//        GraphColoring<String> gc = new GraphColoring<>(parser.getGraph());
//        gc.run();
//        return new TreeMap<>(gc.get());
        return null;
    }

    public void writeSolutionToFile(TreeMap<String, Integer> t, String solnName) {
        int numCols = -1;
        List<String> lines = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : t.entrySet()) {
            if (numCols < entry.getValue()) {
                numCols = entry.getValue();
            }
            lines.add(entry.getKey() + " " + entry.getValue());
        }
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(solnName, false));
            writer.printf("%s" + "%n", numCols);
            for (String line : lines) {
                writer.printf("%s" + "%n" , line);
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
