package org.bitbucket.nhweston.comp333a1a;

import java.util.*;

public class GraphColoring<V> implements Runnable {

    protected Graph<V> graph;
    protected Map<V, Integer> result;

    private long stepsSearchLoop;
    private long stepsInterferenceLoop;

    private int numVertices;
    protected Map<V, Integer> indicesByVertex;
    protected List<V> verticesByIndex;
    private int[] colors;
    private int numColors;
    private int[] colorsOptimal;
    private int numColorsOptimal;
    private Comparator<V> comparator;

    public GraphColoring(Graph<V> graph) {
        this.graph = graph;
    }

    @Override
    public void run() {
        if (result != null) {
            // Already solved.
            return;
        }
        assignIndices();
        colors = new int[numVertices];
        colorsOptimal = new int[numVertices];
        numColors = 0;
        numColorsOptimal = Integer.MAX_VALUE;
        stepsSearchLoop = 0;
        stepsInterferenceLoop = 0;
        search(0);
        result = new Hashtable<>();
        for (int i=0; i<numVertices; i++) {
            result.put(verticesByIndex.get(i), colorsOptimal[i]);
        }
    }

    public Map<V, Integer> get() {
        return result;
    }

    public Comparator<V> comparator() {
        return comparator;
    }

    public int numColorsOptimal() {
        return numColorsOptimal;
    }

    public long stepsSearchLoop() {
        return stepsSearchLoop;
    }

    public long stepsInterferenceLoop() {
        return stepsInterferenceLoop;
    }

    protected void setCols(int vertexId, int color) {
        colors[vertexId] = color;
    }

    protected void setColorsOptimal(int[] colorsOptimal) {
        this.colorsOptimal = Arrays.copyOf(colorsOptimal, colorsOptimal.length);
    }

    private void assignIndices() {
        // Order vertices in descending order of degree.
        numVertices = graph.vertices().size();
        indicesByVertex = new Hashtable<>();
        verticesByIndex = new ArrayList<>(graph.vertices());
        comparator = Comparator
                .comparingInt(graph::degree).reversed()
                .thenComparing(Comparator.comparingInt(Object::hashCode));
        verticesByIndex.sort(comparator);
        for (int i=0; i< verticesByIndex.size(); i++) {
            indicesByVertex.put(verticesByIndex.get(i), i);
        }
    }

    private void search(int vertexId) {
        // Have we found a solution?
        if (vertexId >= numVertices) {
            // Is this solution optimal?
            if (numColors < numColorsOptimal) {
                numColorsOptimal = numColors;
                setColorsOptimal(colors);
            }
            return;
        }
        V vertex = verticesByIndex.get(vertexId);
        // Which colors cannot be assigned to this vertex?
        Collection<Integer> interferences = new HashSet<>();
        for (V adj : graph.adjacencies(vertex)) {
            stepsInterferenceLoop++;
            interferences.add(colors[indicesByVertex.get(adj)]);
        }
        // The search begins...
        for (int i=1; i<=numColors; i++) {
            stepsSearchLoop++;
            if (!interferences.contains(i)) {
                // Tentatively assign this color, then attempt to solve the remainder of the graph.
                setCols(vertexId, i);
                search(vertexId+1);
                // Could this potentially lead to a more optimal solution?
                if (numColors >= numColorsOptimal) {
                    // No need to search this branch further.
                    setCols(vertexId, 0);
                    return;
                }
            }
        }
        // Need a new colour. Could this still lead to a more optimal solution?
        if (numColors + 1 < numColorsOptimal) {
            numColors++;
            setCols(vertexId, numColors);
            search(vertexId+1);
            numColors--;
        }
        setCols(vertexId, 0);
    }

}
