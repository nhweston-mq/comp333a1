package org.bitbucket.nhweston.comp333a1a;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class Liveness {

    public static void main(String[] args) {}

    public Liveness() {}

    public TreeMap<String, Integer> generateSolution(String fInName) {
        Parser parser = new Parser(fInName);
        parser.run();
        GraphColoring<String> gc = new GraphColoring<>(parser.getGraph());
        gc.run();
        TreeMap<String, Integer> result = new TreeMap<>(Comparator.comparingInt(s -> gc.get().get(s)));
        return result;
    }

    public void writeSolutionToFile(TreeMap<String, Integer> t, String solnName) {
        int numCols = -1;
        List<String> lines = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : t.entrySet()) {
            if (numCols < entry.getValue()) {
                numCols = entry.getValue();
            }
            lines.add(entry.getKey() + " " + entry.getValue());
        }
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(solnName, false));
            writer.printf("%s" + "%n", numCols);
            for (String line : lines) {
                writer.printf("%s" + "%n" , line);
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
