package org.bitbucket.nhweston.comp333a1a;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Deque;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser implements Runnable {

    private static final Pattern ASSIGN, DELIM, EXPR, LIVE, MEM, OP, VAR;

    static {
        ASSIGN  =   Pattern.compile(    ":="                                            );
        MEM     =   Pattern.compile(    "(?:mem\\[|])"                                  );
        OP      =   Pattern.compile(    "[+\\-*/]"                                      );
        VAR     =   Pattern.compile(    "[a-zA-z][a-zA-Z0-9]"                           );
        DELIM   =   Pattern.compile(    "(?:" + OP + "|" + MEM + "(?:\\s*))+"           );
        EXPR    =   Pattern.compile(    "(" + VAR + ")\\s*" + ASSIGN + "\\s*(.*)"       );
        LIVE    =   Pattern.compile(    "live-((?:in)|(?:out))((?:\\s+" + VAR + ")*)"   );
    }

    private String filePath;
    private Graph<String> graph;
    private Collection<Collection<String>> subgraphsComplete;
    private Collection<String> live;

    public Parser(String filePath) {
        this.filePath = filePath;
        graph = new AdjacencyList<>();
        live = new HashSet<>();
    }

    @Override
    public void run() {
        Deque<String> lines = readFile();
        while (!lines.isEmpty()) {
            String line = lines.pollLast().trim();
            processLine(line);
        }
    }

    public Graph<String> getGraph() {
        return graph;
    }

    private Deque<String> readFile() {
        System.out.println(LIVE.pattern());
        Deque<String> result = new LinkedList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
            reader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            return result;
        }
        return result;
    }

    private void processLine(String line) {
        Matcher matcher;
        if ((matcher = EXPR.matcher(line)).matches()) {
            // Assignment expression.
            String varAssigned = matcher.group(1);
            String[] valsUsed = DELIM.split(matcher.group(2));
            // Mark the variables on the RHS as used.
            for (String token : valsUsed) {
                token = token.trim();
                if (VAR.matcher(token).matches()) {
                    markUsed(token);
                }
            }
            // Mark the variable on the LHS as assigned.
            markAssigned(varAssigned);
        }
        else if ((matcher = LIVE.matcher(line)).matches()) {
            // Live-in or live-out expression.
            String[] tokens = matcher.group(2).trim().split("\\s+");
            Consumer<String> inOrOut = matcher.group(1).equals("in") ? this::markAssigned : this::markUsed;
            for (String token : tokens) {
                inOrOut.accept(token);
            }
        }
    }

    private void markAssigned(String token) {
        live.remove(token);
    }

    private void markUsed(String token) {
        graph.addVertex(token);
        for (String other : live) {
            graph.addEdge(token, other);
        }
        live.add(token);
    }

}
