package org.bitbucket.nhweston.comp333a1a;

import java.util.Collection;

public interface Graph<T> {

    boolean addVertex(T v);
    boolean removeVertex(T v);
    boolean addEdge(T v1, T v2);
    boolean removeEdge(T v1, T v2);
    boolean containsVertex(T v);
    boolean containsEdge(T v1, T v2);
    int degree(T v);
    Collection<T> vertices();
    Collection<T> adjacencies(T v);

    default void addVertices(Collection<T> collection) {
        for (T v : collection) {
            addVertex(v);
        }
    }

    default void addEdges(T v, Collection<T> collection) {
        for (T adj : collection) {
            addEdge(v, adj);
        }
    }

}
