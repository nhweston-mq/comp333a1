/*
 * Nicholas Weston (44882017)
 * COMP333 A1(a)
 */

package student;

import java.io.*;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

interface Graph<T> {

    boolean addVertex(T v);
    boolean removeVertex(T v);
    boolean addEdge(T v1, T v2);
    boolean removeEdge(T v1, T v2);
    boolean containsVertex(T v);
    boolean containsEdge(T v1, T v2);
    int degree(T v);
    Collection<T> vertices();
    Collection<T> adjacencies(T v);

    default void addVertices(Collection<T> collection) {
        for (T v : collection) {
            addVertex(v);
        }
    }

    default void addEdges(T v, Collection<T> collection) {
        for (T adj : collection) {
            addEdge(v, adj);
        }
    }

}

class AdjacencyList<T> implements Graph<T> {

    private Map<T, Collection<T>> adjacencies;

    public AdjacencyList() {
        adjacencies = new Hashtable<>();
    }

    @Override
    public boolean addVertex(T v) {
        if (adjacencies.containsKey(v)) return false;
        adjacencies.put(v, new HashSet<>());
        return true;
    }

    @Override
    public boolean removeVertex(T v) {
        if (!adjacencies.containsKey(v)) return false;
        adjacencies.remove(v);
        return true;
    }

    @Override
    public boolean addEdge(T v1, T v2) {
        if (!containsVertex(v1) || !containsVertex(v2) || containsEdge(v1, v2)) return false;
        adjacencies.get(v1).add(v2);
        adjacencies.get(v2).add(v1);
        return true;
    }

    @Override
    public boolean removeEdge(T v1, T v2) {
        if (!containsEdge(v1, v2)) return false;
        adjacencies.get(v1).remove(v2);
        adjacencies.get(v2).remove(v1);
        return true;
    }

    @Override
    public boolean containsVertex(T v) {
        return adjacencies.containsKey(v);
    }

    @Override
    public boolean containsEdge(T v1, T v2) {
        Collection<T> temp = adjacencies.get(v1);
        return (temp != null) && temp.contains(v2);
    }

    @Override
    public int degree(T v) {
        Collection<T> temp = adjacencies.get(v);
        return (temp == null) ? -1 : temp.size();
    }

    @Override
    public Collection<T> vertices() {
        return adjacencies.keySet();
    }

    @Override
    public Collection<T> adjacencies(T v) {
        Collection<T> temp = adjacencies.get(v);
        return (temp == null) ? null : new HashSet<>(temp);
    }

}

class GraphColoring<V> implements Runnable {

    protected Graph<V> graph;
    protected Map<V, Integer> result;

    private int numVertices;
    protected Map<V, Integer> indicesByVertex;
    protected List<V> verticesByIndex;
    private int[] colors;
    private int numColors;
    private int[] colorsOptimal;
    private int numColorsOptimal;
    private Comparator<V> comparator;

    public GraphColoring(Graph<V> graph) {
        this.graph = graph;
    }

    @Override
    public void run() {
        if (result != null) {
            // Already solved.
            return;
        }
        assignIndices();
        colors = new int[numVertices];
        colorsOptimal = new int[numVertices];
        numColors = 0;
        numColorsOptimal = Integer.MAX_VALUE;
        search(0);
        result = new Hashtable<>();
        for (int i=0; i<numVertices; i++) {
            result.put(verticesByIndex.get(i), colorsOptimal[i]);
        }
    }

    public Map<V, Integer> get() {
        return result;
    }

    public Comparator<V> comparator() {
        return comparator;
    }

    public int numColorsOptimal() {
        return numColorsOptimal;
    }

    protected void setCols(int vertexId, int color) {
        colors[vertexId] = color;
    }

    protected void setColorsOptimal(int[] colorsOptimal) {
        this.colorsOptimal = Arrays.copyOf(colorsOptimal, colorsOptimal.length);
    }

    private void assignIndices() {
        // Order vertices in descending order of degree.
        numVertices = graph.vertices().size();
        indicesByVertex = new Hashtable<>();
        verticesByIndex = new ArrayList<>(graph.vertices());
        comparator = Comparator
                .comparingInt(graph::degree).reversed()
                .thenComparing(Comparator.comparingInt(Object::hashCode));
        verticesByIndex.sort(comparator);
        for (int i=0; i< verticesByIndex.size(); i++) {
            indicesByVertex.put(verticesByIndex.get(i), i);
        }
    }

    private void search(int vertexId) {
        // Have we found a solution?
        if (vertexId >= numVertices) {
            // Is this solution optimal?
            if (numColors < numColorsOptimal) {
                numColorsOptimal = numColors;
                setColorsOptimal(colors);
            }
            return;
        }
        V vertex = verticesByIndex.get(vertexId);
        // Which colors cannot be assigned to this vertex?
        Collection<Integer> interferences = new HashSet<>();
        for (V adj : graph.adjacencies(vertex)) {
            interferences.add(colors[indicesByVertex.get(adj)]);
        }
        // The search begins...
        for (int i=1; i<=numColors; i++) {
            if (!interferences.contains(i)) {
                // Tentatively assign this color, then attempt to solve the remainder of the graph.
                setCols(vertexId, i);
                search(vertexId+1);
                // Could this potentially lead to a more optimal solution?
                if (numColors >= numColorsOptimal) {
                    // No need to search this branch further.
                    setCols(vertexId, 0);
                    return;
                }
            }
        }
        // Need a new colour. Could this still lead to a more optimal solution?
        if (numColors + 1 < numColorsOptimal) {
            numColors++;
            setCols(vertexId, numColors);
            search(vertexId+1);
            numColors--;
        }
        setCols(vertexId, 0);
    }

}

class Parser implements Runnable {

    private static final Pattern ASSIGN, DELIM, EXPR, LIVE, MEM, OP, VAR;

    static {
        ASSIGN  =   Pattern.compile(    ":="                                            );
        MEM     =   Pattern.compile(    "(?:mem\\[|])"                                  );
        OP      =   Pattern.compile(    "[+\\-*/]"                                      );
        VAR     =   Pattern.compile(    "[a-zA-z][a-zA-Z0-9]"                           );
        DELIM   =   Pattern.compile(    "(?:" + OP + "|" + MEM + "(?:\\s*))+"           );
        EXPR    =   Pattern.compile(    "(" + VAR + ")\\s*" + ASSIGN + "\\s*(.*)"       );
        LIVE    =   Pattern.compile(    "live-((?:in)|(?:out))((?:\\s+" + VAR + ")*)"   );
    }

    private String filePath;
    private Graph<String> graph;
    private Collection<String> live;

    public Parser(String filePath) {
        this.filePath = filePath;
        graph = new AdjacencyList<>();
        live = new HashSet<>();
    }

    @Override
    public void run() {
        Deque<String> lines = readFile();
        while (!lines.isEmpty()) {
            String line = lines.pollLast().trim();
            processLine(line);
        }
    }

    public Graph<String> get() {
        return graph;
    }

    private Deque<String> readFile() {
        Deque<String> result = new LinkedList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
            reader.close();
        }
        catch (IOException e) {
            e.printStackTrace();
            return result;
        }
        return result;
    }

    private void processLine(String line) {
        Matcher matcher;
        if ((matcher = EXPR.matcher(line)).matches()) {
            // Assignment expression.
            String varAssigned = matcher.group(1);
            String[] valsUsed = DELIM.split(matcher.group(2));
            // Mark the variables on the RHS as used.
            for (String token : valsUsed) {
                token = token.trim();
                if (VAR.matcher(token).matches()) {
                    markUsed(token);
                }
            }
            // Mark the variable on the LHS as assigned.
            markAssigned(varAssigned);
        }
        else if ((matcher = LIVE.matcher(line)).matches()) {
            // Live-in or live-out expression.
            String[] tokens = matcher.group(2).trim().split("\\s+");
            Consumer<String> inOrOut = matcher.group(1).equals("in") ? this::markAssigned : this::markUsed;
            for (String token : tokens) {
                inOrOut.accept(token);
            }
        }
    }

    private void markAssigned(String token) {
        live.remove(token);
    }

    private void markUsed(String token) {
        graph.addVertex(token);
        for (String other : live) {
            graph.addEdge(token, other);
        }
        live.add(token);
    }

}

public class Liveness {

    public static void main(String[] args) {}

    public Liveness() {}

    public TreeMap<String, Integer> generateSolution(String fInName) {
        Parser parser = new Parser(fInName);
        parser.run();
        GraphColoring<String> gc = new GraphColoring<>(parser.get());
        gc.run();
        return new TreeMap<>(gc.get());
    }

    public void writeSolutionToFile(TreeMap<String, Integer> t, String solnName) {
        int numCols = -1;
        List<String> lines = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : t.entrySet()) {
            if (numCols < entry.getValue()) {
                numCols = entry.getValue();
            }
            lines.add(entry.getKey() + " " + entry.getValue());
        }
        try {
            PrintWriter writer = new PrintWriter(new FileWriter(solnName, false));
            writer.printf("%s" + "%n", numCols);
            for (String line : lines) {
                writer.printf("%s" + "%n" , line);
            }
            writer.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
